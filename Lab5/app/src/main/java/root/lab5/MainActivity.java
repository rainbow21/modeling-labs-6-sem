package root.lab5;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.radiusEdit)
    EditText radiusEdit;

    @BindView(R.id.F0Edit)
    EditText F0Edit;

    @BindView(R.id.LEdit)
    EditText LEdit;

    @BindView(R.id.alpha0Edit)
    EditText alpha0Edit;

    @BindView(R.id.alphaNEdit)
    EditText alphaNEdit;

    @BindView(R.id.TosEdit)
    EditText TosEdit;

    @BindView(R.id.stepEdit)
    EditText stepEdit;

    @BindView(R.id.tauEdit)
    EditText tauEdit;

    @BindView(R.id.nGraphEdit)
    EditText nGraphEdit;

    @BindView(R.id.graph)
    GraphView graphView;

    @OnClick(R.id.clearGrahpButton)
    public void clearGraph(Button btn) {
        graphView.removeAllSeries();
    }

    @OnClick(R.id.startButton)
    public void startSolve(Button btn) {
        Solver solver = new Solver(new Double[]{
                Double.valueOf(radiusEdit.getText().toString()),
                Double.valueOf(F0Edit.getText().toString()),
                Double.valueOf(LEdit.getText().toString()),
                Double.valueOf(alpha0Edit.getText().toString()),
                Double.valueOf(alphaNEdit.getText().toString()),
                Double.valueOf(TosEdit.getText().toString()),
                Double.valueOf(stepEdit.getText().toString()),
                Double.valueOf(tauEdit.getText().toString()),
                Double.valueOf(nGraphEdit.getText().toString())
        });

        solver.solve5();
        ArrayList<ArrayList<Point>> result = solver.getResult5();
        if (result == null) {
            Toast.makeText(this, "Не удалось построить график", Toast.LENGTH_SHORT).show();
            return;
        }
        // Сколько графиков
        int nGraph = result.size();
        for (int i = 0; i < nGraph; i++) {
            LineGraphSeries s = new LineGraphSeries();
            s.setColor(Color.WHITE);
            if (i == nGraph-1)
                s.setColor(Color.BLACK);
            int nPoint = result.get(i).size();
            for (int j = 0; j < nPoint; j++)
                s.appendData(new DataPoint(result.get(i).get(j).x, result.get(i).get(j).T), true, nPoint);

            graphView.addSeries(s);
        }

        Viewport viewport = graphView.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(solver.getL());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fillDefaultValues();
    }

    private void fillDefaultValues() {
        radiusEdit.setText(String.format(Locale.ENGLISH, "%5.3f", 0.5));
        F0Edit.setText(String.format(Locale.ENGLISH, "%d", 30));
        LEdit.setText(String.format(Locale.ENGLISH, "%d", 5));
        alpha0Edit.setText(String.format(Locale.ENGLISH, "%5.3f", 1e-2));
        alphaNEdit.setText(String.format(Locale.ENGLISH, "%5.3f", 2e-2));
        TosEdit.setText(String.format(Locale.ENGLISH, "%d", 250));
        stepEdit.setText(String.format(Locale.ENGLISH, "%5.3f", 0.1));
        tauEdit.setText(String.format(Locale.ENGLISH, "%d", 5));
        nGraphEdit.setText(String.format(Locale.ENGLISH, "%d", 10));
    }
}
