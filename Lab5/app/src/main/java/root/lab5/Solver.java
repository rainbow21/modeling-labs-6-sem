package root.lab5;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import static java.lang.Math.abs;
import static java.lang.Math.min;


public class Solver {
    private Double radius;  final int radiusIndex = 0;
    private Double F0;      final int F0Index = 1;
    private Double L;       final int LIndex = 2;
    private Double alpha0;  final int alpha0Index = 3;
    private Double alphaN;  final int alphaNIndex = 4;
    private Double Tos;     final int TosIndex = 5;
    private Double step;    final int stepIndex = 6;
    private Double tau;     final int tauIndex = 7;
    private Double countGraph;    int countGraphIndex = 8;
    private final Integer COUNT_PARAM = 9;

    final Double EPS = 0.001;

    public Double getL() {
        return L;
    }

    private TCapacity cTable[] = new TCapacity[] {
        new TCapacity(300.0, 2.544),
        new TCapacity(400.0, 2.615),
        new TCapacity(500.0, 2.656),
        new TCapacity(600.0, 2.689),
        new TCapacity(700.0, 2.717),
        new TCapacity(800.0, 2.748),
        new TCapacity(900.0, 2.783),
        new TCapacity(1000.0, 2.817),
        new TCapacity(1200.0, 2.893),
        new TCapacity(1400.0, 2.977),
        new TCapacity(1600.0, 3.070),
        new TCapacity(1800.0, 3.124),
        new TCapacity(2000.0, 3.270),
        new TCapacity(2200.0, 3.381),
        new TCapacity(2400.0, 3.502),
        new TCapacity(2600.0, 3.640),
        new TCapacity(2800.0, 3.792),
        new TCapacity(3000.0, 3.968)
    };
    private TConductivity kTable[] = new TConductivity[] {
            new TConductivity(300.0, 0.163),
            new TConductivity(400.0, 0.156),
            new TConductivity(500.0, 0.146),
            new TConductivity(600.0, 0.137),
            new TConductivity(700.0, 0.130),
            new TConductivity(800.0, 0.124),
            new TConductivity(900.0, 0.120),
            new TConductivity(1000.0, 0.117),
            new TConductivity(1200.0, 0.114),
            new TConductivity(1400.0, 0.111),
            new TConductivity(1600.0, 0.110),
            new TConductivity(1800.0, 0.109),
            new TConductivity(2000.0, 0.108),
            new TConductivity(2200.0, 0.107),
            new TConductivity(2400.0, 0.107),
            new TConductivity(2600.0, 0.107),
            new TConductivity(2800.0, 0.109),
            new TConductivity(3000.0, 0.108)
    };


    private Double a;
    private Double b;

    private ArrayList<Point> result;
    private ArrayList<ArrayList<Point>> result5;

    public Solver(Double param[]) throws InvalidParameterException {
        if (param.length != COUNT_PARAM)
            throw new InvalidParameterException();
        radius = param[radiusIndex];
        F0 = param[F0Index];
        L = param[LIndex];
        alpha0 = param[alpha0Index];
        alphaN = param[alphaNIndex];
        Tos = param[TosIndex];
        step = param[stepIndex];
        tau = param[tauIndex];
        countGraph = param[countGraphIndex];
    }

    // Из Лабораторной №3
    public void solve3() {
        result = new ArrayList<>();

        // Этап 1. Подсчет a, b
        a = (-1) * (alpha0 * alphaN * L)/(alphaN - alpha0);
        b = (alphaN * L) / (alphaN - alpha0);
        // Количество точек
        int n = (int)Math.round(L/step);
        if (n > 2000) {
            result = null;
            return;
        }
        Double h = step;
        Double h2 = step*step;
        // Массивы температур. 2 т.к. нужно считать точность
        ArrayList<Double> Tcur = new ArrayList<>(), Told;
        for (int i = 0; i < n+1; i++)
            Tcur.add(Tos);

        ArrayList<Coefficients> coefs = new ArrayList<>();

        int iter = 0;
        while (true) {
            // Левое условие
            Coefficients c = new Coefficients(0.0, 0.0, 0.0, 0.0);
            c.A = Xi1_2(Tcur.get(0), Tcur.get(1)) + h2/8 * p1_2(0.0, h) + h2/4 * p(0.0);
            c.B = -(Xi1_2(Tcur.get(0), Tcur.get(1)) - h2/8 * p1_2(0.0, h));
            c.C = F0*h - h2/4*(f1_2(0.0, h) + f(0.0));
            c.D = 0.0;
            coefs.add(c);
            // Общая формула
            for (int i = 1; i < n; i++) {
                c = new Coefficients(0.0, 0.0, 0.0, 0.0);
                c.A = Xi1_2(Tcur.get(i-1), Tcur.get(i));
                c.C = Xi1_2(Tcur.get(i), Tcur.get(i+1));
                c.B = -(c.A + c.C + p(h*i)*h2);
                c.D = f(i*h)*h2;
                coefs.add(c);
            }

            // Правое условие
            c = new Coefficients(0.0,0.0,0.0,0.0);
            c.A = Xi1_2(Tcur.get(Tcur.size()-2), Tcur.get(Tcur.size()-1)) - h2/8 * p1_2(L-h, L);
            c.B = -(Xi1_2(Tcur.get(Tcur.size()-2), Tcur.get(Tcur.size()-1)) + h2/4*p(L) + h2/8*p1_2(L-h, L));
            c.C = h*alphaN * (Tcur.get(Tcur.size()-1) - Tos) + h2/4*(f1_2(L-h, L) + f(L));
            c.D = 0.0;
            coefs.add(c);

            Told = Tcur;
            Tcur = progonka(coefs);
            coefs.clear();
            iter++;

            if (diff(Told, Tcur) < EPS || iter >= 10)
                break;
        }

        n = Tcur.size();
        for (int i = 0; i < n; i++)
            result.add(new Point(i*step, Tcur.get(i)));
    }

    public void solve5() {
        result5 = new ArrayList<>();
        int resCur = 0; // Текущий результат
        // Этап 1. Подсчет a, b
        a = (-1) * (alpha0 * alphaN * L)/(alphaN - alpha0);
        b = (alphaN * L) / (alphaN - alpha0);

        int N = (int)Math.round(L/step);

        double h = step;
        double h2 = step*step;

        ArrayList<Point> answer = new ArrayList<>();
        ArrayList<Point> old_answer = new ArrayList<>();
        for (int i = 0; i < N+1; i++) {
            answer.add(new Point(Tos, Tos));
            old_answer.add(new Point(0.0,0.0));
        }

        while (needIter(old_answer, answer)) {
            int iter = 0;
            ArrayList<Double> T = new ArrayList<>();
            ArrayList<Double> old_T = new ArrayList<>();
            for (int i = 0; i < N+1; i++) {
                T.add(answer.get(i).T);
                old_T.add(0.0);
            }

            ArrayList<Coefficients> coef = new ArrayList<>();
            for (int i = 0; i < N+1; i++)
                coef.add(new Coefficients(0,0,0,0));

            while (true) {
                double T0 = T.get(0);
                double T1 = T.get(1);
                double bufC = C(T0);
                double bufC2 = C1_2(T0, T1);

                coef.get(0).A = tau*(Xi1_2(T0,T1) + h2/8*p1_2(0.0, h) + h2/4*p(0.0)) + h2/4 * C(T.get(0)) + h2/8 *C1_2(T0, T1);
                coef.get(0).B = tau*(h2/8*p1_2(0.0, h) - Xi1_2(T0, T1)) + h2/8 * C1_2(T0, T1);
                coef.get(0).C = tau*F0*h + tau*h2/8*(3*f(0.0) + f(h)) + h2/4*C(T0)*answer.get(0).T + h2/8*C1_2(T0, T1)*(answer.get(0).T + answer.get(1).T);

                for (int i = 1; i < N; i++) {
                    coef.get(i).A = Xi1_2(T.get(i), T.get(i-1)) * tau;                                          // Ai
                    coef.get(i).C = Xi1_2(T.get(i), T.get(i+1)) * tau;                                          // Ci
                    coef.get(i).B = -(coef.get(i).A + coef.get(i).C + p(i*h)*h2 * tau + C(T.get(i)) * h2);   // Bi
                    coef.get(i).D = -(f(i*h) * h2 * tau + C(T.get(i)) * h2 * answer.get(i).T);               // Di
                }

                double Tn = T.get(N);
                double Tn_1 = T.get(N-1);
                coef.get(N).A = -tau*(Xi1_2(Tn, Tn_1) + alphaN*h + h2/4*p(L) + h2/8*p1_2(L-h, L)) + h2/4*C(T.get(N)) + h2/8*C1_2(Tn_1, Tn);
                coef.get(N).B = (Xi1_2(Tn_1, Tn) - h2/8*p1_2(L-h, L))*tau + h2/8*C1_2(Tn_1, Tn);
                coef.get(N).C = -(h*alphaN * Tos + h2/8*(3*f(L) + f(L-h)))*tau + h2/4*C(Tn)*answer.get(N).T + h2/8*C1_2(Tn_1, Tn)*(answer.get(N).T + answer.get(N-1).T);

                old_T = T;
                T = progonka(coef);
                iter += 1;

                if (diff(old_T, T) < EPS || iter > 10)
                    break;
            }
            old_answer = answer;
            answer = new ArrayList<>();
            for (int i = 0; i < N+1; i++)
                answer.add(new Point(h*i, T.get(i)));

            if (resCur % countGraph == 0)
                result5.add(answer);
            resCur++;
        }
        if (result5.isEmpty())
            result5.add(answer);
    }

    public ArrayList<Point> getResult() {
        return result;
    }

    public ArrayList<ArrayList<Point>> getResult5() {
        return result5;
    }

    private ArrayList<Double> progonka(ArrayList<Coefficients> cof) {
        int n = cof.size();

        Double b1 = cof.get(0).A;
        Double c1 = cof.get(0).B;
        Double d1 = cof.get(0).C;

        double ksi[] = new double[n], ita[] = new double[n];
        Double g = b1;
        ksi[0] = -c1/g;
        ita[0] = d1/g;

        for (int i = 1; i < n-1; i++) {
            Double ai = cof.get(i).A;
            Double bi = cof.get(i).B;
            Double ci = cof.get(i).C;
            Double di = cof.get(i).D;

            g = bi + ai * ksi[i-1];
            ksi[i] = -ci/g;
            ita[i] = (di - ai*ita[i-1])/g;
        }

        Double am = cof.get(n-1).A;
        Double bm = cof.get(n-1).B;
        Double dm = cof.get(n-1).C;

        double res[] = new double[n];
        res[n-1] = (dm-am*ita[n-2])/(bm + am*ksi[n-2]);
        for (int i = n-2; i >= 0; i--)
            res[i] = ksi[i]*res[i+1] + ita[i];

        ArrayList<Double> T = new ArrayList<>();
        for (int i = 0; i < n; i++)
            T.add(res[i]);
        return T;
    }
    /*                                      */
    /*      Вспомогательные функциии        */
    /*                                      */
    private Double alpha(Double x) {
        return a/(x - b);
    };

    private Double p(Double x) {
        return 2*alpha(x) / radius;
    }

    private Double p1_2(Double x1, Double x2) {
        return p((x1+x2)/2);
    }

    private Double f(Double x) {
        return 2*alpha(x) / radius * Tos;
    }

    private Double f1_2(Double x1, Double x2) {
        return f((x1+x2)/2.0);
    }

    private Double lambda(Double t) {
        return k(t);
    }

    private Double k(Double t) {
        return Interpolator.interpolation(kTable, t, 1);
    }

    private Double Xi1_2(Double t1, Double t2) {
        return (2 * lambda(t1) * lambda(t2)) / (lambda(t1) + lambda(t2));
    }

    private Double diff(ArrayList<Double> l1, ArrayList<Double> l2) {
        Integer size = min(l1.size(), l2.size());
        Double max = abs((l1.get(0) - l2.get(0))/(l1.get(0)));

        for (int i = 1; i <size; i++) {
            Double d = abs((l1.get(i) - l2.get(i))/(l1.get(i)));
            if (d > max)
                max = d;
        }

        return max;
    }
    private Double C(Double t) {
        return Interpolator.interpolation(cTable, t, 1);
    }
    private Double C1_2(Double T1, Double T2) {
        return C((T1+T2)/2);
    }

    private boolean needIter(ArrayList<Point> l1, ArrayList<Point> l2) {
        int n = l1.size();
        for (int i = 0; i < n; i++)
            if (abs((l1.get(i).T - l2.get(i).T)/l2.get(i).T) > EPS)
                    return true;
        return false;
    }
}
