package root.lab5;
// Коэффициенты метода прогонки
public class Coefficients {
    public Coefficients(double a, double b, double c, double d) {
        A = a;
        B = b;
        C = c;
        D = d;
    }
    public Double A;
    public Double B;
    public Double C;
    public Double D;
}
