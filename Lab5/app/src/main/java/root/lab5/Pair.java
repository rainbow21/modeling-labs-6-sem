package root.lab5;

public interface Pair {
    Double getArg1();
    Double getArg2();
}
