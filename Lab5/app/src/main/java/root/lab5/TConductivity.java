package root.lab5;

public class TConductivity implements Pair {
    public TConductivity(Double t, Double conduct) {
        T = t;
        conductivity = conduct;
    }
    public Double T;            // Температура
    public Double conductivity; // Теплопроводность при заданной температуре

    @Override
    public Double getArg1() {
        return T;
    }

    @Override
    public Double getArg2() {
        return conductivity;
    }
}
