package root.lab3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.series.Series;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.tetaEdit)
    EditText tetaEdit;

    @BindView(R.id.lambda0Edit)
    EditText lambda0Edit;

    @BindView(R.id.radiusEdit)
    EditText radiusEdit;

    @BindView(R.id.F0Edit)
    EditText F0Edit;

    @BindView(R.id.LEdit)
    EditText LEdit;

    @BindView(R.id.alpha0Edit)
    EditText alpha0Edit;

    @BindView(R.id.alphaNEdit)
    EditText alphaNEdit;

    @BindView(R.id.TosEdit)
    EditText TosEdit;

    @BindView(R.id.stepEdit)
    EditText stepEdit;

    @BindView(R.id.graph)
    GraphView graphView;

    @OnClick(R.id.clearGrahpButton)
    public void clearGraph(Button btn) {
        graphView.removeAllSeries();
    }

    private int colors[] = {Color.WHITE, Color.BLACK, Color.RED, Color.GREEN};
    private int index = 0;

    @OnClick(R.id.startButton)
    public void startSolve(Button btn) {
        Solver solver = new Solver(new Double[]{
            Double.valueOf(tetaEdit.getText().toString()),
            Double.valueOf(lambda0Edit.getText().toString()),
                Double.valueOf(radiusEdit.getText().toString()),
                Double.valueOf(F0Edit.getText().toString()),
                Double.valueOf(LEdit.getText().toString()),
                Double.valueOf(alpha0Edit.getText().toString()),
                Double.valueOf(alphaNEdit.getText().toString()),
                Double.valueOf(TosEdit.getText().toString()),
                Double.valueOf(stepEdit.getText().toString())
        });

        solver.solve();
        ArrayList<Point> result = solver.getResult();
        if (result == null) {
            Toast.makeText(this, "Не удалось построить график", Toast.LENGTH_SHORT).show();
            return;
        }
        LineGraphSeries s = new LineGraphSeries();
        s.setColor(colors[index++ % colors.length]);
        int n = result.size();
        for (int i = 0; i < n; i++)
            s.appendData(new DataPoint(result.get(i).x, result.get(i).T), true, result.size());

        graphView.addSeries(s);

        Viewport viewport = graphView.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(solver.getL());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fillDefaultValues();
    }

    private void fillDefaultValues() {
        tetaEdit.setText(String.format(Locale.ENGLISH,"%d", 293));
        lambda0Edit.setText(String.format(Locale.ENGLISH, "%5.3f", 0.1));
        radiusEdit.setText(String.format(Locale.ENGLISH, "%5.3f", 0.1));
        F0Edit.setText(String.format(Locale.ENGLISH, "%d", 100));
        LEdit.setText(String.format(Locale.ENGLISH, "%d", 10));
        alpha0Edit.setText(String.format(Locale.ENGLISH, "%5.3f", 1e-2));
        alphaNEdit.setText(String.format(Locale.ENGLISH, "%5.3f", 1.5e-2));
        TosEdit.setText(String.format(Locale.ENGLISH, "%d", 350));
        stepEdit.setText(String.format(Locale.ENGLISH, "%5.3f", 0.02));
    }
}
