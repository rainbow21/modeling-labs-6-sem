package root.lab3;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import static java.lang.Math.abs;
import static java.lang.Math.min;


public class Solver {
    private Double teta;    final int tetaIndex = 0;
    private Double lambda0; final int lamda0Index = 1;
    private Double radius;  final int radiusIndex = 2;
    private Double F0;      final int F0Index = 3;
    private Double L;       final int LIndex = 4;
    private Double alpha0;  final int alpha0Index = 5;
    private Double alphaN;  final int alphaNIndex = 6;
    private Double Tos;     final int TosIndex = 7;
    private Double step;    final int stepIndex = 8;

    final Double EPS = 0.01;

    public Double getL() {
        return L;
    }

    private Double a;
    private Double b;

    private ArrayList<Point> result;

    public Solver(Double param[]) throws InvalidParameterException {
        if (param.length != 9)
            throw new InvalidParameterException();
        teta = param[tetaIndex];
        lambda0 = param[lamda0Index];
        radius = param[radiusIndex];
        F0 = param[F0Index];
        L = param[LIndex];
        alpha0 = param[alpha0Index];
        alphaN = param[alphaNIndex];
        Tos = param[TosIndex];
        step = param[stepIndex];
    }

    public void solve() {
        result = new ArrayList<>();

        // Этап 1. Подсчет a, b
        a = (-1) * (alpha0 * alphaN * L)/(alphaN - alpha0);
        b = (alphaN * L) / (alphaN - alpha0);
        // Количество точек
        int n = (int)Math.round(L/step);
        if (n > 2000) {
            result = null;
            return;
        }
        Double h = step;
        Double h2 = step*step;
        // Массивы температур. 2 т.к. нужно считать точность
        ArrayList<Double> Tcur = new ArrayList<>(), Told;
        for (int i = 0; i < n+1; i++)
            Tcur.add(Tos);

        ArrayList<Coefficients> coefs = new ArrayList<>();

        int iter = 0;
        while (true) {
            // Левое условие
            Coefficients c = new Coefficients();
            c.A = Xi1_2(Tcur.get(0), Tcur.get(1)) + h2/8 * p1_2(0.0, h) + h2/4 * p(0.0);
            c.B = -(Xi1_2(Tcur.get(0), Tcur.get(1)) - h2/8 * p1_2(0.0, h));
            c.C = F0*h - h2/4*(f1_2(0.0, h) + f(0.0));
            c.D = 0.0;
            coefs.add(c);
            // Общая формула
            for (int i = 1; i < n; i++) {
                c = new Coefficients();
                c.A = Xi1_2(Tcur.get(i-1), Tcur.get(i));
                c.C = Xi1_2(Tcur.get(i), Tcur.get(i+1));
                c.B = -(c.A + c.C + p(h*i)*h2);
                c.D = f(i*h)*h2;
                coefs.add(c);
            }

            // Правое условие
            c = new Coefficients();
            c.A = Xi1_2(Tcur.get(Tcur.size()-2), Tcur.get(Tcur.size()-1)) - h2/8 * p1_2(L-h, L);
            c.B = -(Xi1_2(Tcur.get(Tcur.size()-2), Tcur.get(Tcur.size()-1)) + h2/4*p(L) + h2/8*p1_2(L-h, L));
            c.C = h*alphaN * (Tcur.get(Tcur.size()-1) - Tos) + h2/4*(f1_2(L-h, L) + f(L));
            c.D = 0.0;
            coefs.add(c);

            Told = Tcur;
            Tcur = progonka(coefs);
            coefs.clear();
            iter++;

            if (diff(Told, Tcur) < EPS || iter >= 10)
                break;
        }

        n = Tcur.size();
        for (int i = 0; i < n; i++)
            result.add(new Point(i*step, Tcur.get(i)));
    }

    public ArrayList<Point> getResult() {
        return result;
    }

    private ArrayList<Double> progonka(ArrayList<Coefficients> cof) {
        int n = cof.size();

        Double b1 = cof.get(0).A;
        Double c1 = cof.get(0).B;
        Double d1 = cof.get(0).C;

        double ksi[] = new double[n], ita[] = new double[n];
        Double g = b1;
        ksi[0] = -c1/g;
        ita[0] = d1/g;

        for (int i = 1; i < n-1; i++) {
            Double ai = cof.get(i).A;
            Double bi = cof.get(i).B;
            Double ci = cof.get(i).C;
            Double di = cof.get(i).D;

            g = bi + ai * ksi[i-1];
            ksi[i] = -ci/g;
            ita[i] = (di - ai*ita[i-1])/g;
        }

        Double am = cof.get(n-1).A;
        Double bm = cof.get(n-1).B;
        Double dm = cof.get(n-1).C;

        double res[] = new double[n];
        res[n-1] = (dm-am*ita[n-2])/(bm + am*ksi[n-2]);
        for (int i = n-2; i >= 0; i--)
            res[i] = ksi[i]*res[i+1] + ita[i];

        ArrayList<Double> T = new ArrayList<>();
        for (int i = 0; i < n; i++)
            T.add(res[i]);
        return T;
    }
    /*                                      */
    /*      Вспомогательные функциии        */
    /*                                      */
    private Double alpha(Double x) {
        return a/(x - b);
    };

    private Double p(Double x) {
        return 2*alpha(x) / radius;
    }

    private Double p1_2(Double x1, Double x2) {
        return p((x1+x2)/2);
    }

    private Double f(Double x) {
        return -2*alpha(x) / radius * Tos;
    }

    private Double f1_2(Double x1, Double x2) {
        return f((x1+x2)/2.0);
    }

    private Double lambda(Double t) {
        int p = 2;
        return lambda0 * Math.pow((t/teta), p);
    }

    private Double Xi1_2(Double t1, Double t2) {
        return (2 * lambda(t1) * lambda(t2)) / (lambda(t1) + lambda(t2));
    }

    private Double diff(ArrayList<Double> l1, ArrayList<Double> l2) {
        Integer size = min(l1.size(), l2.size());
        Double max = abs(l1.get(0) - l2.get(0));

        for (int i = 1; i <size; i++) {
            Double d = abs(l1.get(i) - l2.get(i));
            if (d > max)
                max = d;
        }

        return max;
    }
}
