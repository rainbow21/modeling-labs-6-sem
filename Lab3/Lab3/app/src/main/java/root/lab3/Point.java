package root.lab3;

// Точка графика T = T(x)
public class Point {
    public Double x;
    public Double T;

    public Point(Double x, Double T) {
        this.x = x;
        this.T = T;
    }
}
