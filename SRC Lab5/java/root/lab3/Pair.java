package root.lab3;

public interface Pair {
    Double getArg1();
    Double getArg2();
}
