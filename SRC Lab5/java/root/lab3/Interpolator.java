package root.lab3;

import java.util.ArrayList;

public class Interpolator {
    public static Double interpolation(Pair list[], Double x0, int n) {
        sortT(list);
        int begin = getBeginPos(list, x0, n+1);
        double v[] = dividedDiff(list, begin, n+1);

        Double result = 0.0;
        Double p = 1.0;

        for (int i = 0; i < n+1; i++) {
            result += v[i]*p;
            p *= (x0 - list[begin + i].getArg1());
        }
        return result;
    }

    // Сортировка по возростанию, по температуре
    public static void sortT(Pair list[]) {
        int i = 0;
        int n = list.length;

        while (i < n-1) {
            if (i < 0) i++;
            if (list[i+1].getArg1() < list[i].getArg1()) {
                Pair buf = list[i];
                list[i] = list[i+1];
                list[i+1] = buf;
                i--;
            }
            else
                i++;
        }
    }

    public static double[] dividedDiff(Pair list[], int begin, int n) {
        ArrayList<Double> tmp = new ArrayList<>();
        for (int i = 0; i < n; i++)
            tmp.add(list[begin + i].getArg2());

        int posEnd = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - 1 - i; j++) {
                double y2 = tmp.get(posEnd + j + 1);
                double y1 = tmp.get(posEnd + j);
                double x2 = list[begin + i + j + 1].getArg1();
                double x1 = list[begin + j].getArg1();
                tmp.add((y2-y1)/(x2-x1));
            }
            posEnd += n-i;
        }
        double res[] = new double[n];
        int index = 0;
        for (int i = 0; i < n; i++) {
            res[i] = tmp.get(i);
            index += n - i;
        }
        return res;
    }

    // Начальная позиция для интерполяции. n - сколько точек требуется
    public static int getBeginPos(Pair list[], Double x0, int n) {
        int len = list.length;
        if (len < n)
            n = len;

        int pos_after = len-1;
        for (int i = 0; i < len; i++)
            if (list[i].getArg1() > x0) {
                if (i == 0)
                    pos_after = 0;
                else
                    pos_after = i - 1;
                break;
            }

        if (pos_after == len)
            return 0;

        pos_after++;
        int posLeft = pos_after-(n/2);
        int posRight = pos_after + (n - n/2);

        if (len < posRight)
            posLeft -= posRight - len;
        if (posLeft < 0)
            posLeft = 0;
        return posLeft;
    }
}
