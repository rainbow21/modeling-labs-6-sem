package root.lab3;

public class TCapacity implements Pair{
    public TCapacity(Double t, Double cap) {
        T = t;
        capacity = cap;
    }
    public Double T;        // Температура
    public Double capacity; // Теплоемкость при заданной температуре

    @Override
    public Double getArg1() {
        return T;
    }

    @Override
    public Double getArg2() {
        return capacity;
    }
}
